# coding: latin-1
#	UDP - chat
# makes use of threads
# 

import socket
import os
from thread import start_new_thread

####################
#  VARIABLES etc.  #
####################

color = [
	#No Color
	'\033[0m',
	#GREEN  -- send
	'\033[1;32m',
	#PURPLE -- recieve
	'\033[1;35m'
	]

localhost = raw_input("your LAN ipv4 address: ") # address to bind on
host = raw_input("LAN ipv4 address of the reciever: ") # address to send to 

#NOTE: listen_port1 & send_port1 should be the same port , the same goes for listen_port2 & send_port2
listen_port1 = 5554 # listening port
listen_port2 = 5555 # listening port
send_port1 = 5554 # sending port
send_port2 = 5555 # sending port

s = socket.socket(socket.AF_INET , socket.SOCK_DGRAM) #defining a ipv4 UDP socket
#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

##############
#  LISTENER  #
##############
def chat_listener(port):
	while True:
		data, addr = s.recvfrom(2048)
		if data != "":
			print color[0] + "" + color[2] + str(data) + color[1]
			if data == "END":
				os._exit(1)
			if data == "EHLO":
				print "\n --- EHLO recieved . reciever is online --- \n"

############
#  SENDER  #
############
def chat_sender(port):
	s.sendto("EHLO", (host,port)) #hello message
	while True:
		input = raw_input(color[1])
		s.sendto(input,(host,port))
		if input == "END":
			s.close()
			os._exit(1)

##########
#  MAIN  #
##########

os.system("clear") #clears screen
print "\n --- type 'END' to quit the chat --- \n"


if host == "127.0.0.1": # if you are testing on your own PC
	try: # listen on port1 and send on port2
		s.bind((localhost,listen_port1))
		print "Listening on port: " , listen_port1
		start_new_thread(chat_sender,(send_port2,))
		chat_listener(listen_port1)
	except: # if the above didn't work or you are running two instances of this program ,listen on port2 and send on port1
		s.bind((localhost,listen_port2))
		print "Listening on port: " , listen_port2
		start_new_thread(chat_sender,(send_port1,))
		chat_listener(listen_port2)
	finally: #if couldn't bind 
		print color[0]
		print "\n --- Couldn't bind on ports 5554 or 5555 - try changing the ports --- \n"
		os._exit(1)
elif host != "127.0.0.1": #listen on port1 and send on port1
	s.bind((localhost,listen_port1))
	print "Listening on port: " , listen_port1
	start_new_thread(chat_sender,(send_port1,))
	chat_listener(listen_port1)
else:
	print "\n --- ERROR --- "
	print " --- ! please use a valid host ! --- "
	print "quitting.."
	os._exit(1)
